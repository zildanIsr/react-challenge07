import React, { Fragment } from 'react'
import { NavMenu, Header, CarouselComp, AccordionComp, FooterComp, ButtonComp} from '../../components'
import { completeIcon, hrsIcon, iconPrice, iconProfessional, imgService } from '../../assets'
import { ceklisIcon } from '../../assets'
import './home.scss'

const Home = () => {
  return (
    <Fragment>
      <NavMenu />
      <Header />
        <div className="btn-hire">
          <ButtonComp title="Mulai Sewa Mobil"/>
        </div>
      <div>
        {/* Service */}
        <div class="service py-100" id="our">
            <div class="container">
                <div class="row mx-3">
                    <div class="col-md-6">
                        <div class="img-our mx-auto">
                            <img src={imgService} className="img-fluid" alt="service" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="text-our-head my-4">
                            <h2 class="fw-bold">Best Car Rental for any kind of trip in (Lokasimu)!</h2>
                        </div>
                        <div class="desc-our">
                            <p>
                            Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.
                            </p>
                        </div>
                        <div class="list-our my-3">
                            <ul class="ps-0">
                                <li>
                                    <img src={ceklisIcon} className="me-3" alt="ceklis" /> <span> Sewa Mobil Dengan Supir di Bali 12 Jam</span>
                                </li>
                                <li>
                                    <img src={ceklisIcon} className="me-3" alt="ceklis.svg" /> <span> Sewa Mobil Lepas Kunci di Bali 24 Jam</span>
                                </li>
                                <li>
                                    <img src={ceklisIcon} className="me-3" alt="ceklis.svg" /> <span> Sewa Mobil Jangka Panjang Bulanan</span>
                                </li>
                                <li>
                                    <img src={ceklisIcon} className="me-3" alt="ceklis.svg" /> <span> Gratis Antar - Jemput Mobil di Bandara</span>
                                </li>
                                <li>
                                    <img src={ceklisIcon} className="me-3" alt="ceklis.svg" /> <span> Layanan Airport Transfer / Drop In Out</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/* end service */}
        {/* start Why us */}
        <div class="why py-100" id="why">
            <div class="container">
                <div class="head-why mx-3">
                    <h4 class="fw-bold">Why Us</h4>
                </div>
                <p class="mx-3 mt-3 mb-5">Mengapa harus pilih Binar Car Rental?</p>

                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 pe-0">
                            <div class="p-4 rounded border">
                                <img src={completeIcon} class="img-fluid" alt="icon_complete.svg" />
                                <h6 class="my-3 fw-bold">Mobil Lengkap</h6>
                                <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 pe-0">
                            <div class="p-4 rounded border">
                                <img src={iconPrice} class="img-fluid" alt="icon_complete.svg" />
                                <h6 class="my-3 fw-bold">Harga Murah</h6>
                                <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 pe-0">
                            <div class="p-4 rounded border">
                                <img src={hrsIcon} class="img-fluid" alt="icon_complete.svg" />
                                <h6 class="my-3 fw-bold">Layanan 24 Jam</h6>
                                <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 pe-0">
                            <div class="p-4 rounded border">
                                <img src={iconProfessional} class="img-fluid" alt="icon_complete.svg" />
                                <h6 class="my-3 fw-bold">Supir Profesional</h6>
                                <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                            </div>
                        </div>
                    </div>
                  </div>
            </div>
        </div>
        {/* End of Why us */}
        {/* start testimonial */}
        <div class="testimonial mb-5" id="testimonial">
            <CarouselComp />
        </div>
        {/* end testimonial */}
        {/* start rent */}    
            <div class="sewa mt-100">
                <div class="container rounded">
                    <div class="d-flex flex-column justify-content-center text-center">
                        <div className='text-title'>
                            <h2 class="text-white">Sewa Mobil di (Lokasimu) Sekarang</h2>
                        </div>
                        <div className='content-desc'>
                            <p class="text-white text-lighter">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                        <div className='button-rent'>
                            <ButtonComp title="Mulai Sewa Mobil"/>
                        </div>
                    </div>
                </div>
            </div>
        {/* end of start rent */}
        {/* start FAQ */}
        <div className="container mt-100">
            <div className="row">
                <div className="col-md-6">
                    <div>
                        <h3 className='fw-bold mb-3'>Frequently Asked Question</h3>
                    </div>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                    </div>
                </div>
                <div className="col-md-6">
                <AccordionComp />
                </div>
            </div>
        </div>
        {/* end of FAQ */}
        {/* Footer */}
        <div className='mt-100'>
            <FooterComp />
        </div>
        {/* end footer */}
      </div>
    </Fragment>
  )
}

export default Home