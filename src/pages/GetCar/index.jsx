import React, { useEffect, useState } from 'react'
import { FooterComp, Header, NavMenu, CardCar } from '../../components'
import './getCar.scss'
import FormCar from './../../components/organisms/FormCar/index';
import axios from 'axios'

const GetCar = ( ) => {

  const [car, setCar] = useState([])
  const [Data, setForm] = useState({})
  // const [newData, setNewData] = useState([])

  const FilterCars = (car, Data) =>{
    const newCars = filterCarByAvailable(car)
    const filterUser = Data

    const newDateTime = filterUser.Date + "T" + filterUser.Time + ":00.000Z"

    return newCars.filter((car) => car.availableAt <= newDateTime && car.capacity >= filterUser.capacity)
  }

  const filterCarByAvailable = () => {
    return car.filter((car) => car.available === true)
  }

  const getCarAPI = async() => {
    try {
      const datas = await axios.get('https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json')
      setCar(datas.data)
    } catch (error) {
      console.log(error);
    }
  }

  const getFormData = (form) => {
    const data = form
    console.log(data);
    setForm(data)
  }


  useEffect(
    () => getCarAPI
  , [])

  return (
    <div className='position-relative'>
      <NavMenu />
      <Header />
      <div className='mt-10s'>
        <FormCar storeForm={getFormData}/>
      </div>
      <div className='mt-100 container'>
        <CardCar cars={FilterCars(car, Data)} />
      </div>
      <div className='mt-100'>
        <FooterComp />
      </div>
    </div>
  )
}

export default GetCar