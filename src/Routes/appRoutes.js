import React from 'react'
import {
    BrowserRouter,
    Routes,
    Route,
  } from "react-router-dom";
import { Home, Cars } from '../pages';


const appRoutes = () => {
  return (
    <BrowserRouter>
        <Routes> 
            <Route path="/" element={<Home />} />
            <Route path="/cars" element={<Cars />} />
        </Routes>
    </BrowserRouter>

  )
}

export default appRoutes