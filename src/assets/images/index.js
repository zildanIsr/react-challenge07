import Logo from './logo.svg';
import carImage from './img_car.png';
import imgService from './img_service.png';
import profile_1 from './profile_1.png';
import profile_2 from './profile_2.png';

export { Logo, carImage, imgService, profile_1, profile_2 };