import ceklisIcon from './ceklis_icon.png';
import completeIcon from './icon_complete.svg'
import hrsIcon from './icon_24hrs.svg'
import iconPrice from './icon_price.svg'
import iconProfessional from './icon_professional.svg'
import star from './star.svg'
import iconFacebook from './icon_facebook.svg'
import iconInstagram from './icon_instagram.svg'
import iconTwitter from './icon_twitter.svg'
import iconMail from './icon_mail.svg'
import iconTwitch from './icon_twitch.svg'
import fiUsers from './fi_users.svg'
import fiSetting from './fi_settings.svg'
import fiCalender from './fi_calendar.svg'


export { ceklisIcon, completeIcon, hrsIcon, iconPrice, iconProfessional, star, iconFacebook, iconInstagram, iconTwitter, iconMail, iconTwitch, fiUsers, fiCalender, fiSetting };