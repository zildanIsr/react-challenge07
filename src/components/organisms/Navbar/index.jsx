import React from 'react'
import { Navbar, NavItem, NavLink, NavbarBrand, NavbarToggler, Nav, Collapse, Button } from 'reactstrap'
import { Logo } from '../../../assets'
import './navmenu.scss'

const NavMenu = () => {
  return (
    <div>
      <Navbar
        expand="md"
        light
        container
        full
        className="navmenu"
      >
        <NavbarBrand href="/">
          <img src={Logo} alt="logo.png"  />
        </NavbarBrand>
        <NavbarToggler onClick={function noRefCheck(){}} />
        <Collapse navbar>
          <Nav
            className="ms-auto"
            navbar
          >
            <NavItem className='my-auto me-2'>
              <NavLink href="#our">
                Our Service
              </NavLink>
            </NavItem>
            <NavItem className='my-auto me-2'>
              <NavLink href="#why">
                Why Us
              </NavLink>
            </NavItem>
            <NavItem className='my-auto me-2'>
              <NavLink href="#testimonial">
                Testimonial
              </NavLink>
            </NavItem>
            <NavItem className='my-auto me-2'>
              <NavLink href="#FAQ">
                FAQ
              </NavLink>
            </NavItem>
            <NavItem className='my-auto me-2'>
              <NavLink href="#">
                <div>
                  <Button
                    color="success"
                    size='md'
                  >
                    Register
                  </Button>
                </div>
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  )
}

export default NavMenu