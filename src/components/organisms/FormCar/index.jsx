import React, { useState} from 'react'
import { Row, Form, Col, Button, FormGroup, Label, Input } from 'reactstrap'
import './form.scss'

const FormCar = ({storeForm}) => {

    const [form, setForm] = useState({})

  return (
    <div className='container forminput'>
        <Form
        >
            <Row
                className='pt-3'
            >
                <Col md={3}>
                    <FormGroup>
                      <Label for="driver">
                        Tipe Driver
                      </Label>
                      <Input onChange={(e) => setForm({
                          ...form,
                          Driver : e.target.value
                      })}
                        id="driver"
                        name="driver"
                        type="select"
                      >
                        <option value="Dengan Driver">
                          Dengan Driver
                        </option>
                        <option value="Tanpa Driver">
                          Tanpa Driver
                        </option>
                      </Input>
                    </FormGroup>
                </Col>
                <Col md={3}>
                    <FormGroup>
                      <Label for="Date">
                        Tanggal
                      </Label>
                      <Input onChange={(e) => setForm({
                          ...form,
                          Date : e.target.value
                      })}
                        id="Date"
                        name="date"
                        placeholder="Pilih Tanggal"
                        type="date"
                      />
                    </FormGroup>
                </Col>
                <Col md={2}>
                    <FormGroup>
                      <Label for="time">
                        Waktu Jemput/Ambil
                      </Label>
                      <Input onChange={(e) => setForm({
                          ...form,
                          Time : e.target.value
                      })}
                        id="time"
                        name="time"
                        placeholder="time placeholder"
                        type="time"
                      />
                    </FormGroup>
                </Col>
                <Col md={3}>
                    <FormGroup>
                      <Label for="capacity">
                        Jumlah Penumpang
                      </Label>
                      <Input onChange={(e) => setForm({
                          ...form,
                          capacity : e.target.value
                      })}
                        id="capacity"
                        name="capacity"
                        placeholder="Penumpang"
                        type="number"
                        min={1}
                        max={6}
                      />
                    </FormGroup>
                </Col>
                <Col md={1}>
                <Button onClick={() => storeForm(form)}
                    color="success"
                    size="sm"
                    className='mt-40s'
                >
                    Cari Mobil
                </Button>
                </Col>
            </Row>
        </Form>
    </div>
  )
}

export default FormCar