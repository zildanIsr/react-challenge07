import React from 'react'
import { UncontrolledAccordion, AccordionItem, AccordionHeader, AccordionBody } from 'reactstrap'

const AccordionComp = () => {
  return (
    <div>
        <div className='faqAccordion'>
        <UncontrolledAccordion
            stayOpen
        >
            <AccordionItem className='mb-3'>
            <AccordionHeader targetId="1">
                Apa saja syarat yang dibutuhkan?
            </AccordionHeader>
            <AccordionBody accordionId="1">
                <strong>
                This is the first item's accordion body.
                </strong>
                You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the{' '}
                <code>
                .accordion-body
                </code>
                , though the transition does limit overflow.
            </AccordionBody>
            </AccordionItem >
            <AccordionItem className='mb-3'>
            <AccordionHeader targetId="2">
                Berapa hari minimal sewa mobil lepas kunci?
            </AccordionHeader>
            <AccordionBody accordionId="2">
                <strong>
                This is the second item's accordion body.
                </strong>
                You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the{' '}
                <code>
                .accordion-body
                </code>
                , though the transition does limit overflow.
            </AccordionBody>
            </AccordionItem>
            <AccordionItem className='mb-3'>
            <AccordionHeader targetId="3">
                Berapa hari sebelumnya sabaiknya booking sewa mobil?
            </AccordionHeader>
            <AccordionBody accordionId="3">
                <strong>
                This is the third item's accordion body.
                </strong>
                You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the{' '}
                <code>
                .accordion-body
                </code>
                , though the transition does limit overflow.
            </AccordionBody>
            </AccordionItem>
            <AccordionItem className='mb-3'>
            <AccordionHeader targetId="4">
                Apakah Ada biaya antar-jemput?
            </AccordionHeader>
            <AccordionBody accordionId="4">
                <strong>
                This is the third item's accordion body.
                </strong>
                You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the{' '}
                <code>
                .accordion-body
                </code>
                , though the transition does limit overflow.
            </AccordionBody>
            </AccordionItem>
            <AccordionItem className='mb-3'>
            <AccordionHeader targetId="5">
                Bagaimana jika terjadi kecelakaan
            </AccordionHeader>
            <AccordionBody accordionId="5">
                <strong>
                This is the third item's accordion body.
                </strong>
                You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the{' '}
                <code>
                .accordion-body
                </code>
                , though the transition does limit overflow.
            </AccordionBody>
            </AccordionItem>
        </UncontrolledAccordion>
        </div>
    </div>
  )
}

export default AccordionComp