import Slider from "react-slick";
import React, { useMemo } from 'react'
import CardComp from "../../atoms/cards/index.jsx"
import { profile_1, profile_2} from '../../../assets'
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const CarouselComp = () => {
    const settings = {
        className: "center",
        dots: true,
        centerMode: true,
        infinite: true,
        centerPadding: "60px",
        slidesToShow: 3,
        speed: 500,
        initialSlide: 0,
        responsive: [
            {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
            },
            {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                initialSlide: 2
            }
            },
            {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
            }
        ]
      }; 

      const slidedata = useMemo(
          () => [
              {
                  img: profile_1,
                  score: 5,
                  desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro doloribus sequi vero, iste laborum adipisci quos ducimus deleniti, natus saepe sapiente culpa. Sit quo vero facilis odit impedit non sequi.",
                  name: "Jhon Doe 32, Bromo"
              },
              {
                img: profile_2,
                score: 4,
                desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro doloribus sequi vero, iste laborum adipisci quos ducimus deleniti, natus saepe sapiente culpa. Sit quo vero facilis odit impedit non sequi.",
                name: "Jhon Doe 32, Bromo"
              },
              {
                img: profile_1,
                score: 5,
                desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro doloribus sequi vero, iste laborum adipisci quos ducimus deleniti, natus saepe sapiente culpa. Sit quo vero facilis odit impedit non sequi.",
                name: "Jhon Doe 32, Bromo"
              },
              {
                img: profile_1,
                score: 5,
                desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro doloribus sequi vero, iste laborum adipisci quos ducimus deleniti, natus saepe sapiente culpa. Sit quo vero facilis odit impedit non sequi.",
                name: "Jhon Doe 32, Bromo"
              }
          ]
      )


  return (
    <div>
        <div className="text-center mb-5">
            <h3 className="fw-bold">Testimonial</h3>
            <p >Berbagai review positif dari para pelanggan kami</p>
        </div>
        <Slider {...settings}>
            {slidedata.map((v =>
                <div>
                    <CardComp 
                    img={v.img}
                    score={v.score} 
                    desc={v.desc}
                    name={v.name} />
                </div>
            ))}
        </Slider>
    </div>
  )
}

export default CarouselComp