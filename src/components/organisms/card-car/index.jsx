import React from 'react'
import { Card, CardImg, CardBody, CardTitle, CardText, Button} from 'reactstrap'
import { fiUsers, fiCalender, fiSetting } from '../../../assets'
import './card-car.scss'

const CardCar = ({cars}) => {

    const imgPath = (cars) => {
        let text = cars

        let url = "https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/public" + text.slice(1, 999)
        return url
    }

  return (
    <div>
        <div className="d-flex flex-wrap justify-content-center">   
            {cars.map((cars, i) => (
                <div key={i} className='card-car'>
                    <Card key={i}
                     className="m-2"
                     
                    >
                      <CardImg
                        className='img-card'
                        alt="Card image cap"
                        src={imgPath(cars.image)}
                        top
                        width="100%"
                      />
                      <CardBody>
                        <CardTitle
                            className='fw-bold'
                        >
                          {cars.manufacture}
                        </CardTitle>
                        <CardText
                            className='fw-bold'
                            tag="h5"
                        >
                          Rp. {cars.rentPerDay} / hari
                        </CardText>
                        <CardText>
                          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dignissimos doloribus quisquam in quas sapiente corrupti, nihil enim impedit provident. Ea modi aliquid eveniet quas debitis laborum, unde optio autem ratione.
                        </CardText>
                        <CardText>
                          <img src={fiUsers} className="me-1" alt="" />  {cars.capacity} orang
                        </CardText>
                        <CardText>
                            <img src={fiSetting} className="me-1" alt="" /> {cars.transmission}
                        </CardText>
                        <CardText>
                            <img src={fiCalender} className="me-1" alt="" /> Tahun {cars.year}
                        </CardText>
                        <Button
                            color='success'
                        >
                          Pilih Mobil
                        </Button>
                      </CardBody>
                    </Card>
                </div>
            ))}
        </div>
    </div>
  )
}

export default CardCar