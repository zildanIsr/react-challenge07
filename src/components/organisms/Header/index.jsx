import React from 'react'
import { carImage } from '../../../assets'
import './Header.scss'

const Header = () => {
  return (
    <div>
      <div className="home" id="home">
        <div className="container py-100">
              <div className="d-flex flex-wrap">
                  <div className="left w-50">
                      <div className="text-head">
                          <h1 className="fw-bold mb-3">Sewa Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
                      </div>
                      <div className="text-desc">
                          <p className="mb-3 w-75">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                      </div>
                  </div>
                  <div className="right w-50">
                      <div className="car-img">
                          <img src={carImage} className="img-fluid" alt="car-rental-banner-1.jpg" />
                      </div>                    
                  </div>
              </div>
          </div>
      </div>
    </div>
  )
}

export default Header