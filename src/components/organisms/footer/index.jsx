import React from 'react'
import './footer.scss'
import { InstagramLink, FacebookLink, TwitterLink, MailLink, TwitchLink } from '../../atoms';
import { Logo } from '../../../assets'

const FooterComp = () => {
  return (
    <div>
        <div className='footer'>
            <div className='container'>
                <div className='row'>
                    <div className="col-md-3">
                      <div className="d-flex flex-column">
                        <div>
                          <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                        </div>
                        <div>
                          <p>binarcarrental@gmail.com</p>
                        </div>
                        <div>
                          <p>081-233-334-808</p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="d-flex flex-column linked">
                        <div className='mb-2'>
                          <a href="#our">Our Service</a>
                        </div>
                        <div className='mb-2'>
                          <a href="#Why">Why Us</a>
                        </div>
                        <div className='mb-2'>
                          <a href="#testimonial">Testimonial</a>
                        </div>
                        <div className='mb-2'>
                          <a href="#FAQ">FAQ</a>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div>
                        <p>Connect with us</p>
                      </div>
                      <div>
                        <FacebookLink />
                        <InstagramLink />
                        <TwitterLink />
                        <MailLink />
                        <TwitchLink />
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div>
                        <p>Copyright Binar 2022</p>
                      </div>
                      <div>
                        <img src={Logo} alt="logo.png" />
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default FooterComp