import React from 'react'
import { iconTwitch } from '../../../assets'
import { Link } from 'react-router-dom'

const TwitchLink = () => {
  return (
    <Link to="https://www.youtube.com/watch?v=dQw4w9WgXcQ">
        <img src={iconTwitch} className="me-3" alt="Twitch.png" />
    </Link>
  )
}

export default TwitchLink