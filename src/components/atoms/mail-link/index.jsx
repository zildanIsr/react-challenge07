import React from 'react'
import { iconMail } from '../../../assets'
import { Link } from 'react-router-dom'

const MailLink = () => {
  return (
    <Link to="https://www.youtube.com/watch?v=dQw4w9WgXcQ">
        <img src={iconMail} className="me-3" alt="Mail.png" />
    </Link>
  )
}

export default MailLink