import React from 'react'
import { iconInstagram } from '../../../assets'
import { Link } from 'react-router-dom'

const InstagramLink = () => {
  return (
    <Link to="https://www.youtube.com/watch?v=dQw4w9WgXcQ">
        <img src={iconInstagram} className="me-3" alt="instagram.png" />
    </Link>
  )
}

export default InstagramLink