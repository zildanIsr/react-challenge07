import React from 'react'
import { FormGroup, Label, Input } from 'reactstrap'

const SelectFrom = (props) => {

    const optionSelect = (datas) => {
        let option = []
        for (let i = 0; i < datas.length; i++) {
            option.push(
                <option key={datas[i]}>
                    {datas[i]}
                </option>
            )
        }

        return option
    }

  return (
    <FormGroup>
      <Label for={props.for}>
        {props.title}
      </Label>
      <Input
        id={props.id}
        name={props.name}
        placeholder={props.title}
        type="select"
      >
        {optionSelect(props.data)}
      </Input>
    </FormGroup>
  )
}

export default SelectFrom