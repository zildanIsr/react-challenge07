import React, { useState } from 'react'
import { Dropdown, DropdownItem, DropdownToggle, DropdownMenu} from 'reactstrap'

const DropdownComp = (props) => {

    const [dropdownOpen, setDropdownOpen] = useState(false);

    const toggle = () => setDropdownOpen(!dropdownOpen);

    const loopItem = (datas) => {
        let item = []
        let key = 1
        for (let i = 0; i < datas.item; i++) {
            item.push(
                <DropdownItem key={key++}>
                    {datas.data[i]}
                </DropdownItem>
            )
        }

        return item
    }

  return (
    <div>
        <Dropdown isOpen={dropdownOpen} toggle={toggle}>
          <DropdownToggle caret >
            {props.title}
          </DropdownToggle>
          <DropdownMenu
          >
            {loopItem(props)}
          </DropdownMenu>
        </Dropdown>
    </div>
    )
}

export default DropdownComp