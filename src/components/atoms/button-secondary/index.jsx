import React from 'react'
import { Link } from 'react-router-dom'

const ButtonComp = (props) => {
  return (
    <Link to="/cars" className="btn btn-success hire" role="button" data-bs-toggle="button">{props.title}</Link>
  )
}

export default ButtonComp