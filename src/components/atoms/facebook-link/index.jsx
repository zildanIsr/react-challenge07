import React from 'react'
import { iconFacebook } from '../../../assets'
import { Link } from 'react-router-dom'

const FacebookLink = () => {
  return (
    <Link to="https://www.facebook.com/">
        <img src={ iconFacebook } className="me-3" alt="facebook.png" />
    </Link>
  )
}

export default FacebookLink