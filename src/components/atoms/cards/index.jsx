import React from 'react'
import { star } from '../../../assets'
import './card.scss'

const CardComp = (props) => {

  const starRating = (rating) => {
    let stars = []
    for (let i = 0; i < rating; i++) {
      stars.push(
        <img src={star} alt="star" />
      )
    }
    return stars
  }

  return (
    <div>
      <div className="card-container mx-3">
        <div className="d-flex align-items-center card-content">
          <div className="flex-shrink-0 mx-4">
            <img src={props.img} alt="image.png" />
          </div>
          <div className="flex-grow-1">
            <div className="mb-2 d-inline-flex">
              {starRating(props.score)}
            </div>
            <div className="text-desc pe-2">
              <p>{props.desc}</p>
            </div>
            <div className="name fw-400">
              <p>{props.name}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CardComp;