import React from 'react'
import { iconTwitter } from '../../../assets'
import { Link } from 'react-router-dom'

const TwitterLink = () => {
  return (
    <Link to="https://www.youtube.com/watch?v=dQw4w9WgXcQ">
        <img src={iconTwitter} className="me-3" alt="instagram.png" />
    </Link>
  )
}

export default TwitterLink